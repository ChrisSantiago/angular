import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PersonajesService} from '../../servicios/personajes.service';


@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html'
})
export class BusquedaComponent implements OnInit {
  personajes: any = {};
  busqueda: string;
  constructor( private activatedRoute: ActivatedRoute,
              private _personajesService: PersonajesService
            ) {
            this.activatedRoute.params.subscribe( params => {
            this.busqueda = params['termino'];
            console.log(this.busqueda);
            this.personajes = _personajesService.buscarPersonaje(params['termino']);
            console.log(this.personajes);
          });
        }

  ngOnInit() {
  }

}
