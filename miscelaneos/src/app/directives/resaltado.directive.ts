import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  //ElementRef para moder interactuar con estilos
  constructor( private el: ElementRef) {
    console.log("Directiva llamada");
    // el.nativeElement.style.backgroundColor = "yellow";
  }

  //Recie el nuevo color de appResaltado
@Input("appResaltado") nuevoColor: string;

//Que quieres escuchar, se puede poner un alias
  @HostListener('mouseenter') mouseEntro(){
    this.resaltar( this.nuevoColor || 'yellow');
    // this.el.nativeElement.style.backgroundColor = "yellow";
  }

  @HostListener('mouseleave') mouseSalio(){
    this.resaltar( null );
    // this.el.nativeElement.style.backgroundColor = null;
  }

  private resaltar (color: string){
    this.el.nativeElement.style.backgroundColor = color;
  }

}
