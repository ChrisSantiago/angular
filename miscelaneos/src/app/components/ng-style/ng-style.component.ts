import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `
    <p [style.fontSize.px]="tamano">
    Hola mudno... esto es una etiqueta HTML
    </p>
    <Button class="btn btn-primary mr-2" (click)="tamano = tamano + 5" >
      <i class="fa fa-plus"></i>
    </Button>

    <Button class="btn btn-primary" (click)="tamano = tamano - 5" >
      <i class="fa fa-minus"></i>
    </Button>
  `,
  styles: []
})
export class NgStyleComponent implements OnInit {

  color: string = 'blue';
  tamano: number = 20;
  constructor() { }

  ngOnInit() {
  }

}
