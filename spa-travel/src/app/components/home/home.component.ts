import { Component } from '@angular/core';
import { CountriesService, Place } from '../../services/countries.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {
  places:Place [] = [];
  constructor( private countriesService: CountriesService) {
    this.places = this.countriesService.getAllCountries();
    console.log(this.places);
  }


}
