import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-place-tarject',
  templateUrl: './place-tarject.component.html'
})
export class PlaceTarjectComponent implements OnInit {
  @Input() place: any = {};
  @Input() id: number;
  constructor( private router: Router) { }

  ngOnInit() {
  }

detail(){
  this.router.navigate(['/detail-personaje', this.id])
}

}
