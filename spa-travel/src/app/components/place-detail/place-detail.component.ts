import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountriesService } from '../../services/countries.service';
@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.component.html',
  styles: []
})
export class PlaceDetailComponent implements OnInit {
placeDetail: any ={};
  constructor( private activatedRoute: ActivatedRoute,
               private countriesService: CountriesService) {
    this.activatedRoute.params.subscribe( params =>{
      this.placeDetail = this.countriesService.getPlace(params['id']);
      console.log(this.placeDetail);
    })
  }

  ngOnInit() {
  }

}
