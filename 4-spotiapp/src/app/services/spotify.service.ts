import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//Manipula la data del servicio, para filtar solo la que deseamos
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http:HttpClient ) {
    console.log('Spotify esta listo');

  }

//Funcion generica para construir la peticion y mandarla
  getQuery( query: string){
    const url = `https://api.spotify.com/v1/${ query }`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQASU0DmJIxEdmNMNMzd5mmO0sVcggzq24E0rjaoe84Tzqw4uqD_m_KvETyzW0PprL6Rsaq0rF6jCyamj38'
    });

    return this.http.get(url, { headers });
  }

  getNewRelaces(){
    // const headers = new HttpHeaders({
    //   'Authorization': 'Bearer BQAXX292-wp_XbBBi99CqPnLuRqYHRxJggjTMnBQPTOx-Ets4fs97bP-Cul2xf-lQbmLFrezyA7bknPrruY'
    // });

//Ejecuta la funcion para ser la llamadaal servicio y filtar lo que queramos
    return this.getQuery('browse/new-releases?limit=20').pipe( map( (data: any) =>{
        return data['albums'].items;
      }));
  }

  getSingers( termino: string ){
    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`).pipe( map( (data: any )=> data['artists'].items));
    // return this.http.get(`https://api.spotify.com/v1/search?q=${ termino }&type=artist&limit=10`, { headers })
    //   .pipe( map( (data: any) => data['artists'].items));
  }

  getSinger( id: string ){
    return this.getQuery(`artists/${ id }`);
  }

  getTopTracksSinger( id: string ){
    return this.getQuery(`artists/${ id }/top-tracks?country=ES`).pipe( map ( ( data: any )=> data['tracks']));
  }
}
