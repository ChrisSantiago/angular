import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html'
})
export class TarjetasComponent{
  // Variable que se obtiene en del html
  @Input() items: any[] = [];


  constructor( private router: Router) { }

//Funcion para para comprara el tipo de objecto que esta regresando, ya se para getNewRelaces o para getSingers
// y mandar para ver los detalles del artista
  verArtista( item: any){
    let artistaId;

    if( item.type === 'artist'){
      artistaId = item.id; //getSingers
    } else {
      artistaId = item.artists[0].id; //  getNewRelaces
    }

    console.log(artistaId);
    //Se pasa por rparametro id a la url de artista
    this.router.navigate([ '/artist', artistaId ]);
  }


}
