import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: []
})
export class ArtistaComponent {

  singer: any = {};
  loading: boolean;
  topTracks: any [] = [];
  constructor( private activatedRoute: ActivatedRoute,
               private spotifyService: SpotifyService) {
    this.loading = true;
    //Se obtiene el ID de artista para detalle
    this.activatedRoute.params.subscribe( params =>{

      //Detalles del artista
      this.getSinger( params['id']);

      //Tracks del artista
      this.getTopTracksSinger( params['id']);
    });
  }

  //Funcion para llamar al servicio y obtener la datapara el detalle de artista
  getSinger( id: string){
    this.loading = true;
    this.spotifyService.getSinger( id ).subscribe( data =>{
      console.log(data);
      this.singer = data;
      this.loading = false;
    });
  }

//Funcion para llamar y obtener los tracks del servicio
  getTopTracksSinger( id: string ){
    this.spotifyService.getTopTracksSinger( id )
      .subscribe( topTracks =>{
        console.log( topTracks);
        this.topTracks = topTracks;
      });
  }

}
