import { Component, OnInit } from '@angular/core';
import {SpotifyService} from '../../services/spotify.service';
// import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  // paises: any[] = [];
  // constructor( private http: HttpClient) {
  //   console.log('Constructor del home llamado')
  //   this.http.get('https://restcountries.eu/rest/v2/lang/es').subscribe( resp =>{
  //     this.paises = resp;
  //     console.log(resp);
  //   })
  //
  // }
  nuevasCanciones: any[] = [];
  loading: boolean;
  error: boolean
  mensajeError: string;
  constructor( private spotify: SpotifyService) {
    //Observador para obtener la data del servicio
    this.loading = true;
    this.error = false;

    //Llama el servicio y obtiene la data te los nuevos lanzamientos
    this.spotify.getNewRelaces().subscribe( ( data: any )  =>{
    console.log(data);
    this.nuevasCanciones = data;
    this.loading = false;

    // En caso que surja un error en el consumo del servicio
  }, (errorServicio)=>{
    this.loading = false;
    this.error = true;
    console.log(errorServicio);
    this.mensajeError = errorServicio.error.error.message;
  });
  }

  ngOnInit() {
  }

}
