import { Component, OnInit } from '@angular/core';
import {PersonajesService, Personaje} from '../../servicios/personajes.service';
//Poder pasar parametro a una pagina, se importa Router
import { Router } from '@angular/router';

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html'
})
export class PersonajesComponent implements OnInit {

  personajes:Personaje[] = [];
  constructor( private _personajesService:PersonajesService,
               private router:Router) {

  }

  ngOnInit() {
    this.personajes = this._personajesService.getPersonajes();
    //console.log(this.personajes);
  }

  //Funcion para pasar parametro
  verPersonaje1( idx: number){
    this.router.navigate( ['/personaje', idx] );
  }

}
