import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//Output va de la mano con EventEmitter
import { Router } from '@angular/router';
//Input una propiedad sera recibida desde afuera
@Component({
  selector: 'app-personaje-tarjeta',
  templateUrl: './personaje-tarjeta.component.html'
})
export class PersonajeTarjetaComponent implements OnInit {

  //la personaje puede venir desde afuera
  @Input() personaje: any = {};
  @Input() index: number;

  @Output() personajeSeleccionado: EventEmitter<number>;
  constructor( private router: Router) {
    this.personajeSeleccionado = new EventEmitter();
  }

  ngOnInit() {
  }

  verPersonaje(){
    //console.log( this.index);
     this.router.navigate( ['/personaje', this.index] );
    //this.personajeSeleccionado.emit( this.index );
  }

}
