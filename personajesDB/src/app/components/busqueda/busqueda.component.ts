import { Component, OnInit } from '@angular/core';
import { PersonajesService } from '../../servicios/personajes.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html'
})
export class BusquedaComponent implements OnInit {
  termino: string;
  personajes: any[] = [];
  constructor( private activatedRoute: ActivatedRoute,
               private _personajesService: PersonajesService,
               private router: Router) {



  }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params =>{
       this.termino = params['busqueda'];
      this.personajes = this._personajesService.buscarPersoajes( params['busqueda']);
      console.log(this.personajes);
      });
  }

  // verPersonajeBusqueda( idx: number){
  //   this.router.navigate( ['/personaje', idx] );
  // }

}
