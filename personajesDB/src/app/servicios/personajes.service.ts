import { Injectable } from '@angular/core';

@Injectable()
export class PersonajesService {
  private personajes:Personaje[] = [
    {
          nombre: "Son Goku",
          bio: "Son Goku es un saiyan natal del planeta Vegeta cuyo verdadero nombre es Kakarotto. Nació en el año 737 justo cuando su padre Barduck luchaba contra la tiranía de Freeza. En sus primeros días logró alcanzar las dos unidades de fuerza de combate, un nivel superior a la media humana.",
          img: "assets/img/aquaman.png",
          aparicion: "Dragon ball",
          poder:"Kamehameha"
        },
        {
              nombre: "Vegeta",
              bio: "Vegeta nació alrededor del año 732. Su padre, el Rey Vegeta, era el rey de los Saiyan, convirtiendo a Vegeta en su príncipe. En su juventud, Vegeta vio cómo su padre tenía que dejar su orgullo de lado frente al Dios de la Destrucción, Beerus, para proteger a su especie.",
              img: "assets/img/batman.png",
              aparicion: "Dragon ball Z",
              poder:"Doble Cañón Galic"
        },
        {
              nombre: "Son Gohan",
              bio: "Gohan es el primer hijo de Son Goku y lleva el nombre del abuelo adoptivo de su padre. Gohan hereda muchos poderes y con el tiempo sobrepasa incluso a su padre. Debido a los distintos orígenes de sus padres, Gohan es mucho más fuerte que si hubiera sido sólo humano o sólo saiyajin, de hecho es el primer híbrido Saiyajin/humano.",
              img: "assets/img/daredevil.png",
              aparicion: "Dragon ball Z",
              poder:"Masenko"
        },
        {
              nombre: "Piccolo",
              bio: "Piccolo sería engendrado por el Rey Demonio Piccolo como último recurso al ser derrotado por Goku. Este heredaría todas sus características. Durante sus primeros 3 años de vida se desarrolló rápidamente, llegando a aparecer como adolescente en el 23º Torneo Mundial de las Artes Marciales.",
              img: "assets/img/hulk.png",
              aparicion: "Dragon ball",
              poder:"Makankosappo"
        },
        {
              nombre: "Krilin",
              bio: "Es un artista marcial de baja estatura y carente de nariz. Lleva la cabeza rapada por su entrenamiento como monje en el templo de Oorin y seis puntos tatuados en su frente (que más tarde desaparecerían al dejarse crecer el pelo).",
              img: "assets/img/linterna-verde.png",
              aparicion: "Dragon ball",
              poder:"Kienzan"
        },
        {
              nombre: "Tenshinhan",
              bio: "Tenshinhan nació en el año 733, durante su niñez fue entrenado por el Maestro Tsuru y en ocasiones por Tao Pai Pai, aunque sus métodos eran bastante crueles. No se sabe nada con certeza acerca de sus padres, solo se sabe que es descendiente de la raza conocida como tríclope (tres ojos), pero es nacido en la Tierra.",
              img: "assets/img/spiderman.png",
              aparicion: "Dragon ball",
              poder:"Kikoho"
        },
        {
              nombre: "Son Goku",
              bio: "Son Goku es un saiyan natal del planeta Vegeta cuyo verdadero nombre es Kakarotto. Nació en el año 737 justo cuando su padre Barduck luchaba contra la tiranía de Freeza. En sus primeros días logró alcanzar las dos unidades de fuerza de combate, un nivel superior a la media humana.",
              img: "assets/img/wolverine.png",
              aparicion: "Dragon ball",
              poder:"Kamehameha"
        }
    ];

  constructor() {
    console.log("Listo para usar el servicio");
  }

  getPersonajes():Personaje[]{
    return this.personajes;
  }

  getPersonaje( idx: string):Personaje{
    return this.personajes[idx];
  }

  buscarPersoajes( termino: string ): Personaje[]{
    let personasjeArr: Personaje[] = [];
    termino = termino.toLowerCase();
    for(let i = 0; i < this.personajes.length; i++){
      let personaje = this.personajes[i];
      let nombre = personaje.nombre.toLowerCase();
      if(nombre.indexOf( termino ) >= 0){
        personaje.id = i;
        personasjeArr.push( personaje );
      }
    }

    return personasjeArr;
  }
}

export interface Personaje{
  nombre: string;
  bio: string;
  img: string;
  aparicion: string;
  poder: string;
  id?: number;
};
