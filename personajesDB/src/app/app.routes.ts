import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AboutComponent} from './components/about/about.component';
import {PersonajesComponent} from './components/personajes/personajes.component';
import {PersonajeComponent} from './components/personaje/personaje.component';
import { BusquedaComponent } from './components/busqueda/busqueda.component';
// Esta clas ee para agregar las rutas, se tiene que importa el componente y añaduir en ROUTES y exportar
//hacia app.modules

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'personajes', component: PersonajesComponent },
  { path: 'personaje/:id', component: PersonajeComponent },
  { path: 'busqueda/:busqueda', component: BusquedaComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
